const express = require('express')
const path = require('path')
const compress = require('compression')
const cors = require('cors')

const app = express()
const PORT = process.env.PORT || 3000

app.use(compress())
app.use(express.static(path.resolve(__dirname, 'dist/')))
app.use(cors())
app.use(function(req, res, next) {
    req.header("Access-Control-Allow-Origin", "*");
    req.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, 'dist/', 'index.html'))
})

app.listen(PORT, function () {
  console.log(`Listening on port ${PORT}`)
})
