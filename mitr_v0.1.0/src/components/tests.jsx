import React from 'react'
import tests from '../Tests.json'
import { Link } from 'react-router-dom'
import HomeLink from './homeLink'

export default class Tests extends React.Component {
  render () {
    var locArray = Object.keys(tests)
    let buttons = null
    buttons = locArray.map((i, name) => {
      return (
        <Link to={`/tests/${i}`}>
          <li className='flex-item bus-item'>
            <img src={`/icons/${i.toLowerCase()}.svg`} className='tests-icon' /><br />
            {i}
          </li>
        </Link>
      )
    })

    return (
      <div className='bus-page'>
      <HomeLink />
        <ul className='flex-container bus-container'>
          {buttons}
        </ul>
      </div>
    )
  }
}
