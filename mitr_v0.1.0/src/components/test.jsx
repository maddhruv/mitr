import React from 'react'
import HomeLink from './homeLink'
import Tests from '../Tests.json'

export default class Test extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      id: props.match.params.test
    }
  }
  render () {
    var routes = Object.entries(Tests[this.state.id])
    routes = routes.map((name, i) => {
      return (
        <p className='route-item'>
          {name[0]}: {name[1].replace('_', ' ')}
        </p>
      )
    })
    return (
      <div className='bus-page'>
        <HomeLink />
        <ul className='flx-container route-sec'>
          {routes}
        </ul>
      </div>
    )
  }
}
