import React from 'react'
import HomeLink from './homeLink'
import faculty from '../Faculty.json'
import { Link } from 'react-router-dom'

export default class Branch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      branch: (props.match.params.branch),
      id: null
    }
  }

  render () {
    var facultyNames = null
    facultyNames = Object.keys(faculty[this.state.branch]).map((name, i) => {
      return (
        <Link to={`/faculty/${this.state.branch}/${name}`}>
          <li className='flex-item teacher-item'>
             {name.replace('_', ' ')}
          </li>
        </Link>
      )
    })

    return (
      <div className='faculty-page'>
        <HomeLink /><br />
        <h1 className='heading'>{this.state.branch} - Branch Faculty List</h1>
        <ul className='flex-container faculty-container'>
          {facultyNames}
        </ul>
      </div>
    )
  }
}
