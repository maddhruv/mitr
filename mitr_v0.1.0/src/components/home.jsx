import React from 'react'
import { Link } from 'react-router-dom'

import Speech from './speech'

import Map from '../images/map.svg'
import Doctor from '../images/doctor.svg'
import Test from '../images/test.svg'
import Pills from '../images/pills.svg'

export default class Home extends React.Component {
  constructor () {
    super()
    var audioElement = document.createElement('audio')
    audioElement.setAttribute('src', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217538/bubble-2.mp3')

    this.sound = () => {
      audioElement.play()
    }
  }

  render () {
    return (
      <div className='page'>
        <Link onClick={this.sound} to='/locate'>
          <div className='locate el'>
            <img src={Map} className='locate-icon' /><br />
            Map
          </div>
        </Link>
        <Speech />
        <Link onClick={this.sound} to='/tests'>
          <div className='doctor el'>
            <img src={Test} className='doctor-icon' /><br />
            Test<br />Facilities
          </div>
        </Link>
        <Link onClick={this.sound} to='/faculty'>
          <div className='pills el'>
            <img src={Doctor} className='pills-icon' /><br />
            Doctor
          </div>
        </Link>
        <Link onClick={this.sound} to='/medicines'>
          <div className='test el'>
            <img src={Pills} className='test-icon' /><br />
            Medicines
          </div>
        </Link>
      </div>
    )
  }
}
