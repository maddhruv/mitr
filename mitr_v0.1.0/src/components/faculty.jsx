import React from 'react'
import { Link } from 'react-router-dom'
import faculty from '../Faculty.json'
import HomeLink from './homeLink'

export default class Faculty extends React.Component {
  render () {
    var departments = Object.keys(faculty).map((name, i) => {
      return (
        <Link to={`/faculty/${name}`}>
          <li className='flex-item faculty-item'>
            {name.replace('_', ' ')}
          </li>
        </Link>
      )
    })
    return (
      <div className='faculty-page'>
        <HomeLink />
        <ul className='flex-container faculty-container'>
          {departments}
        </ul>
      </div>
    )
  }
}
