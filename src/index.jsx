import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './components/home'
import Locate from './components/locate'
import Faculty from './components/faculty'
import General from './components/general'
import Tests from './components/tests'
import Location from './components/location'
import Branch from './components/branch'
import Doctor from './components/doctor'
import Test from './components/test'
import Medicines from './components/medicines'

import './styles.css'

class App extends React.Component {
  render () {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/locate' component={Locate} />
          <Route path='/location/:loc' component={Location} />
          <Route exact path='/general' component={General} />
          <Route exact path='/tests' component={Tests} />
          <Route path='/tests/:test' component={Test} />
          <Route exact path='/faculty' component={Faculty} />
          <Route exact path='/faculty/:branch' component={Branch} />
          <Route path='/faculty/:branch/:faculty' component={Doctor} />
          <Route exact path='/medicines' component={Medicines} />
        </Switch>
      </BrowserRouter>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('main'))
