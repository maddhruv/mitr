import React from 'react'
import { Link } from 'react-router-dom'
import locations from '../Location.json'
import HomeLink from './homeLink'

export default class Locate extends React.Component {
  render () {
    var locArray = Object.keys(locations)
    let buttons = null
    let imgSrc = null
    buttons = locArray.map((i, name) => {
      imgSrc = locations[i].icon
      name = i.replace('_', ' ')
      return (
        <Link to={`/location/${i}`}>
          <li className='flex'><img className='icon' src={`/icons/${locations[i].icon}.png`} /><br />{name}</li>
        </Link>
      )
    })

    return (
      <div className='locate-page'>
        <HomeLink />
        <ul className='flex-container'>
          {buttons}
        </ul>
      </div>
    )
  }
}
