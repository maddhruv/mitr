import React from 'react'
import medicines from '../Medicines.json'
import HomeLink from './homeLink'

export default class Medicines extends React.Component {
  render () {
    var items = Object.entries(medicines)
    let prices = null
    prices = items.map((i, name) => {
      var itemName = i[0].replace(/_/g, ' ')
      var price = i[1]
      return (
        <li className='flex-item cafe-item'>{itemName} - {price}</li>
      )
    })
    return (
      <div class='cafe-page'>
        <HomeLink />
        <ul className='flex-container cafe-container'>
          {prices}
        </ul>
      </div>
    )
  }
}
