import React from 'react'
import locations from '../Location.json'
import Voice from '../bin/voice'
import HomeLink from './homeLink'
import MdAddLocation from 'react-icons/lib/md/add-location'

export default class Location extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      location: decodeURIComponent(props.match.params.loc)
    }
    this.address = locations[this.state.location].address
    if (this.address) {
      Voice(this.address)
    }
  }

  render () {
    console.log(this.state.location)
    let map = <img className='map' src={`/maps/${this.state.location}.png`} />
    let place = this.state.location.replace('_', ' ')
    let address = this.address

    return (
      <div className='locate-page'>
        <HomeLink /><br />
        <div className='flex-container'>
          <div class='flex-cell-1 card'>
            <h1 class='location-place'>{place}</h1>
            <h2 className='address'>{address}</h2>
          </div>
          <div class='flex-cell-2'>
            {map}
          </div>
        </div>
      </div>
    )
  }
}
