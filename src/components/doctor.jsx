import React from 'react'
import faculty from '../Faculty.json'
import HomeLink from './homeLink'
import { Link } from 'react-router-dom'

export default class Doctor extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      branch: props.match.params.branch,
      faculty: props.match.params.faculty
    }
    this.teacher = faculty[this.state.branch][this.state.faculty]
  }
  render () {
    let avatar = <img className='avatar' src={`/avatar/${this.state.faculty}.jpg`} />
    return (
      <div className='faculty-page'>
        <HomeLink />
        <div className='profile'>
          <h1 className='teacherName'>{this.state.faculty.replace('_', ' ')}</h1>
          {avatar}
          <h2>Department: {this.teacher.department}</h2>
          <h2>{this.teacher.degree}</h2>
          <h2>Email: {this.teacher.email}</h2>
          <h2>Mobile: {this.teacher.phone}</h2>
          <h2>Timings: {this.teacher.timing}</h2>
          <Link to={`/location/${this.teacher.sitting}`} className='sitting'>Sitting: {this.teacher.sitting.replace('_', ' ')}</Link>
        </div>
      </div>
    )
  }
}
