import React from 'react'

import think from '../bin/ai'
import action from '../bin/actions'

import Mic from '../images/mic.svg'

export default class Speech extends React.Component {
  constructor () {
    super()
    this.state = {
      listening: false
    }
    this.listen = this.listen.bind(this)
  }

  listen () {
    if (!('webkitSpeechRecognition' in window)) {
      console.log('not')
    } else {
      this.setState({listening: true})
      var recognition = new webkitSpeechRecognition()
      recognition.continuos = false
      recognition.interimResults = false

      recognition.lang = 'en-US'
      recognition.start()

      recognition.onresult = function (e) {
        console.log(e.results[0][0].transcript)
        think(e.results[0][0].transcript, action)
        this.setState({listening: false})
        recognition.stop()
      }.bind(this)

      recognition.onerror = function (e) {
        recognition.stop()
      }
    }
  }

  render () {
    let flag = null

    if (this.state.listening) {
      flag = <div className='loading'></div>
    } else {
      flag = <div onClick={this.listen}><img src={Mic} className='mic-icon' /><br />Speak</div>
    }
    return (
      <div className='speech el'>
        {flag}
      </div>
    )
  }
}
