import React from 'react'
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import FaHome from 'react-icons/lib/fa/home'

export default class HomeLink extends React.Component {
  constructor () {
    super()
    this.state = {
      redirect: false
    }
  }
  render () {
    let redirect = null
    setTimeout(() => {
      this.setState({redirect: true})
    }, 10000)

    if (this.state.redirect) {
      redirect = <Redirect to='/' />
    }

    return (
      <Link className='homelink' to='/'>
        <FaHome />
        <Switch>
          <Route>
            {redirect}
          </Route>
        </Switch>
      </Link>
    )
  }
}
