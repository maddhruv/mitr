import Voice from './voice'
import medicines from '../Medicines.json'

export default function action (action, speech = null, param = null) {
  console.log(action)
  switch (action) {
    case 'location':
      // var address = locations[param].floor + ', ' + locations[param].building
      // Voice(address)
      window.location.href = '/location/' + param
      break
    case 'bus':
      window.location.href = '/bus/' + param
      break
    case 'faculty':
      console.log(param.replace(' ', '_'))
      window.location.href = '/teacher/' + param.replace(' ', '_')
      break
    case 'medicines':
      console.log(param)
      var name = param.replace(/_/g, ' ')
      Voice(`The price of ${name} is ${medicines[param]} rupees`)
      break
    default:
      Voice(speech)
  }
}
