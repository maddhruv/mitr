import * as $ from 'jquery'

export default function think (query, callback) {
  $.ajax({
    type: 'POST',
    url: 'https://api.dialogflow.com/v1/query',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    headers: {
      'Authorization': 'Bearer 698e7277827a478fb754e184198e486a'
    },
    data: JSON.stringify({query: query, lang: 'en', sessionId: 'yaydevdiner', 'timezone': 'America/New_York'}),
    success: (data) => {
      var params = null
      var action = data.result.action
      var speech = data.result.speech
      if (data.result.parameters.param != null) {
        params = data.result.parameters.param
      }
      callback(action, speech, params)
    },
    error: () => {
      console.log('error')
    }
  })
}
